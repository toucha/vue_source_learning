/**
 * Vue的公共接口,作用：
 * 将原型被包装后的Vue导入，然后通过initGlobalAPI()给Vue添加各种静态属性和方法，并且同时对原型进行了修改，在原型基本上又添加了
 * $isServer 和 $ssrContext
 * 
 */

// 核心：从Vue的出生文件导入Vue
import Vue from './instance/index'
// 初始化的全局API
import { initGlobalAPI } from './global-api/index'
// 获取boolean型变量，来判断是不是SSR(服务器端渲染上下文(SSR context))
import { isServerRendering } from 'core/util/env'
// 猜测为虚拟DOM创建
import { FunctionalRenderContext } from 'core/vdom/create-functional-component'

/**
 * 将Vue 构造函数作为参数，调用了initGlobalAPI() 来初始化 Vue 的公共接口，包括：
 * Vue.util
 * Vue.set
 * Vue.delete
 * Vue.nextTick
 * Vue.options
 * Vue.use
 * Vue.mixin
 * Vue.extend
 * Vue.extend
 * asset相关接口：配置在 src/core/config.js 中
 */
initGlobalAPI(Vue)
// 在vue的原型上定义只读属性: $isServer，该属性代理了来自 core/util/env.js 文件的 isServerRendering 方法
Object.defineProperty(Vue.prototype, '$isServer', {
  get: isServerRendering
})
// 在 Vue.prototype 上添加 $ssrContext 这个只读属性
Object.defineProperty(Vue.prototype, '$ssrContext', {
  get () {
    /* istanbul ignore next */
    return this.$vnode && this.$vnode.ssrContext
  }
})

// 在Vue构造函数上定义 FunctionalRenderContext 静态属性，且FunctionalRenderContext 属性的值为来自core/vdom/create-functional-component.js 文件的 FunctionalRenderContext
// 在Vue构造函数上定义该属性的原因是为了在SSR中使用
Object.defineProperty(Vue, 'FunctionalRenderContext', {
  value: FunctionalRenderContext
})

// Vue.version 存储了当前 Vue 的版本号
Vue.version = '__VERSION__'

export default Vue
