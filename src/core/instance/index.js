/**
 * 该文件只是构造函数，Vue 原型对象的声明分散在当前目录的多个文件中:
 * init.js：._init()
 * state.js：.$data .$set() .$delete() .$watch()
 * render.js：._render() ...
 * events.js：.$on() .$once() .$off() .$emit()
 * lifecycle.js：._mount() ._update() .$forceUpdate() .$destroy()
 * 
 */

import { initMixin } from './init'
import { stateMixin } from './state'
import { renderMixin } from './render'
import { eventsMixin } from './events'
import { lifecycleMixin } from './lifecycle'
import { warn } from '../util/index'
// 定义 Vue 构造函数
function Vue (options) {
  // 使用安全模式来提醒你要使用 new 操作符来调用 Vue
  if (process.env.NODE_ENV !== 'production' &&
    !(this instanceof Vue)
  ) {
    warn('Vue is a constructor and should be called with the `new` keyword')
  }
  // 构造函数接收参数options ，然后调用 this._init(options),_init方法挂载在Vue的原型对象上
  this._init(options)
}


/** 
 * 将 Vue 作为参数传递各个初始化方法，然后在Vue.prototype上挂载各种方法和属性
 * 每个Mixin 方法的作用其实就是包装 Vue.prototype，在其上挂载一些属性和方法
 * */ 
// initMixin负责定义_init()这个内部方法
initMixin(Vue)
// stateMixin负责初始化 $data、$props、$set、$delete、$watch 5个方法
stateMixin(Vue)
// eventsMixin负责初始化 $on、$once、$off、$emit 4个方法
eventsMixin(Vue)
// lifecycleMixin负责初始化 _update、$forceUpdate、$destory
lifecycleMixin(Vue)
// renderMixin负责初始化 其它一系列方法(markOnce、toNumber、toString、renderList、renderSlot、looseEqual、looseIndexOf、
// renderStatic、resolveFilter、checkKeyCodes、bindObjectProps、createTextVNode、createEmptyVNode、resolveScopedSlots、
// bindObjectListeners)、$nextTick 、_render
renderMixin(Vue)


// 导出 Vue(这是才是Vue的初生地)
export default Vue
