/* @flow */

import config from '../config'
import { initProxy } from './proxy'
import { initState } from './state'
import { initRender } from './render'
import { initEvents } from './events'
// 用于开启性能追踪
import { mark, measure } from '../util/perf'
import { initLifecycle, callHook } from './lifecycle'
import { initProvide, initInjections } from './inject'
import { extend, mergeOptions, formatComponentName } from '../util/index'

let uid = 0

// flow语法的写法：定义的参数的类型，如Vue的参数类型是：对象
// initMixin的作用是就在Vue的原型上添加_init方法，进行内部初始化
export function initMixin (Vue: Class<Component>) {

  // 在Vue原型上定义了一个内部初始化的方法_init(), 当执行 new Vue(options)时调用该方法
  Vue.prototype._init = function (options?: Object) {
    // 声明常量 vm，指向当前的this 即vue实例
    const vm: Component = this
    // 定义_uid计数(定义在了initMixin方法上面)，在实例化Vue时会++
    vm._uid = uid++

    let startTag, endTag
    /**
     * 在非生产环境下，并且 config.performance(性能追踪) 和 mark 都为真时执行
     * 在/util/perf.js中了解mark方法的用处
     */
    if (process.env.NODE_ENV !== 'production' && config.performance && mark) {
      startTag = `vue-perf-start:${vm._uid}`
      endTag = `vue-perf-end:${vm._uid}`
      // 打标：供measure()使用，用来计算两个标记之间代码执行的性能
      mark(startTag)
    }

    // 是否是Vue实例的标识，如果是Vue实例，则不需要被observe(在core/observer/index.js的observe方法中体现)
    vm._isVue = true
    // 第一步：options参数的处理，其中_isComponent为options的内部对象，在 Vue 创建组件的时候才会有的, 所以初始化时该对象不存在
    if (options && options._isComponent) {
      // 合并options
      initInternalComponent(vm, options)
    } else {
      // Vue实例会在代码运行后增加很多新的属性和方法进去(三个参数)
      // 这一步最中包含了数据规范化，数据合并等处理
      vm.$options = mergeOptions(
        // resolveConstructorOptions：解析构造函数的options
        // vue实例时： 返回的结果为：{components:{KeepAlive,Transition,TransitionGroup}, directives:{model,show}, filters:Object.create(null), _base:Vue
        resolveConstructorOptions(vm.constructor),
        // 第二个参数为调用Vue实例化构造函数时传入的对象
        options || {},
        // 第三个参数为 Vue 实例
        vm
      ) 
      /* 上面的代码相当于：
      vm.$options = mergeOptions(
        // resolveConstructorOptions(vm.constructor)
        {
          components: {
            KeepAlive
            Transition,
            TransitionGroup
          },
          directives:{
            model,
            show
          },
          filters: Object.create(null),
          _base: Vue
        },
        // options || {}
        {
          el: '#app',
          data: {
            test: 1
          }
        },
        vm
      )
      */
    }
    // 第二步：执行代理(非生产环境)，将_renderProxy对象挂载到vm上,通过执行initProxy方法才使用 _renderProxy有真正的值
    if (process.env.NODE_ENV !== 'production') {
      initProxy(vm)
    } else {
      // 如果是生产环境则直接将_renderProxy对象挂载到vm上，实际上此时的_renderProxy仅仅是定义，并示初始化，其值还是undefined
      vm._renderProxy = vm
    }
    // 将vm实例同时挂在_self属性上
    vm._self = vm

    // 第三步：vm的生命周期相关变量初始化
    initLifecycle(vm)

    // 第四步：vm的事件监听初始化(在beforeCreate之前对生命周期/事件/render 进行了初始化)
    initEvents(vm)
    // 渲染
    initRender(vm)
    // 调用生命周期钩子函数，将初始化inject, state,provide
    callHook(vm, 'beforeCreate')
    initInjections(vm) // resolve injections before data/props

    // 第五步：vm的状态初始化，在beforeCreate和creted之间，主要是对data/props/computed/watch等进行监听，因此也是Vue实例create的关键
    // initState包含了initProps、initMethods、initData、initComputed 以及 initWatch
    initState(vm)
    initProvide(vm) // resolve provide after data/props
    // 调用生命周期钩子函数（来源于lifecycle）
    // created 生命周期钩子则恰恰是等待 initInjections、initState 以及 initProvide 执行完毕之后才被调用，但由于上面未执行任何挂载操作，
    // 所以此时在 created 中是不能访问DOM的，即不能访问 $el
    callHook(vm, 'created')

    /* 在非生产环境下，并且 config.performance 和 mark 都为真时执行 */
    if (process.env.NODE_ENV !== 'production' && config.performance && mark) {
      vm._name = formatComponentName(vm, false)
      // 打标：供measure()使用，用来计算两个标记之间代码执行的性能
      mark(endTag)
      measure(`vue ${vm._name} init`, startTag, endTag)
    }

    // 第六步：render & mount
    if (vm.$options.el) {
      // 最后是调用 .$mount() 方法来“启动”Vue
      // 在 web-runtime-with-compiler.js 和 web-runtime.js 中，定义了 Vue.prototype.$mount()方法
      vm.$mount(vm.$options.el)
    }
  }
}

export function initInternalComponent (vm: Component, options: InternalComponentOptions) {
  const opts = vm.$options = Object.create(vm.constructor.options)
  // doing this because it's faster than dynamic enumeration.
  const parentVnode = options._parentVnode
  opts.parent = options.parent
  opts._parentVnode = parentVnode

  const vnodeComponentOptions = parentVnode.componentOptions
  opts.propsData = vnodeComponentOptions.propsData
  opts._parentListeners = vnodeComponentOptions.listeners
  opts._renderChildren = vnodeComponentOptions.children
  opts._componentTag = vnodeComponentOptions.tag

  if (options.render) {
    opts.render = options.render
    opts.staticRenderFns = options.staticRenderFns
  }
}
// 解析构造者的options， Ctor: Vue的构造函数(其作用是来获取当前实例构造者的 options 属性的)
export function resolveConstructorOptions (Ctor: Class<Component>) {
  // 也即 Vue.options， 因为如果Ctor为Vue的构造函数，则Ctor的options指向Vue的options
  /**
   * vue通过new初始化时，options对象为：
   *  beforeCreate： [...],
   *  components: {...},
   *  destroyed: [...],
   *  directives: {...},
   *  filters: {},
   *  _base: Vue
   */
  let options = Ctor.options
  // super属性是通过 Vue.extend构造实例时，子类才有的属性, 所以通过new Vue创建的实例中不存在super属性
  if (Ctor.super) {
    // 递归获取到Ctor自身的options (即如果构建者即有super属性说明它是个子类，通过递归获取其父类的options，Ctor.super指向父类)
    const superOptions = resolveConstructorOptions(Ctor.super)
    // Vue构造函数上的options,如directives,filters,....
    const cachedSuperOptions = Ctor.superOptions
    // 两个options值不等，说明"父类"的options改变过了,将自身的superOptions属性替换为最新的，然后再检查自身的Options是否发生了变化。
    if (superOptions !== cachedSuperOptions) {
      // super option changed,
      // need to resolve new options.
      Ctor.superOptions = superOptions
      // #4976 
      const modifiedOptions = resolveModifiedOptions(Ctor)
      // update base extend options
      if (modifiedOptions) {
        extend(Ctor.extendOptions, modifiedOptions)
      }
      options = Ctor.options = mergeOptions(superOptions, Ctor.extendOptions)
      if (options.name) {
        options.components[options.name] = Ctor
      }
    }
  }
  // 如果是vue实例就直接返回实例上的options
  return options
}

// G114行：
function resolveModifiedOptions (Ctor: Class<Component>): ?Object {
  let modified
  // 自身的options
  const latest = Ctor.options
  // 构造"自身"时传入的options
  const extended = Ctor.extendOptions
  // 执行Vue.extend时封装的"自身"options，这个属性就是方便检查"自身"的options有没有变化
  // 遍历当前构造器上的options属性，如果在"自身"封装的options里没有，则证明是新添加的。执行if内的语句。调用dedupe方法，
  // 最终返回modified变量(即”自身新添加的options“)
  const sealed = Ctor.sealedOptions
  for (const key in latest) {
    if (latest[key] !== sealed[key]) {
      if (!modified) modified = {}
      modified[key] = dedupe(latest[key], extended[key], sealed[key])
    }
  }
  return modified
}
// latest: 自身的options
// extended: 传入的options
// 
function dedupe (latest, extended, sealed) {
  // compare latest and sealed to ensure lifecycle hooks won't be duplicated
  // between merges
  if (Array.isArray(latest)) {
    const res = []
    sealed = Array.isArray(sealed) ? sealed : [sealed]
    extended = Array.isArray(extended) ? extended : [extended]
    for (let i = 0; i < latest.length; i++) {
      // push original options and not sealed options to exclude duplicated options
      if (extended.indexOf(latest[i]) >= 0 || sealed.indexOf(latest[i]) < 0) {
        res.push(latest[i])
      }
    }
    return res
  } else {
    return latest
  }
}
