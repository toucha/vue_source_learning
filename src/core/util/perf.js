import { inBrowser } from './env'

export let mark
export let measure
// 开发环境下扫行，生产环境下则什么也不做
if (process.env.NODE_ENV !== 'production') {
  // 定义变量perf, 浏览器环境下则是window.performance，否则则为false
  const perf = inBrowser && window.performance
  // 在此进行一系列判断
  if (
    perf &&
    perf.mark &&
    perf.measure &&
    perf.clearMarks &&
    perf.clearMeasures
  ) {
    // 使用给定的 tag，通过mark方法打一个标记(实际上是对 performance.mark()的封装)
    mark = tag => perf.mark(tag)
    // 对performance.measure()的封装，作用是通过mark打标后，计算两个标记间代码的性能
    measure = (name, startTag, endTag) => {
      perf.measure(name, startTag, endTag)
      perf.clearMarks(startTag)
      perf.clearMarks(endTag)
      perf.clearMeasures(name)
    }
  }
}
